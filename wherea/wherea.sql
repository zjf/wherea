BEGIN;
	/*
   DROP DATABASE IF EXISTS wherea;
  CREATE DATABASE wherea DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
  USE wherea;
	*/

	CREATE TABLE `notice` (
	  `id` INTEGER PRIMARY KEY NOT NULL,
		`title` varchar(255) not null,
		`content` varchar(1024) not null,
		`is_back` boolean default 0,
		`back_str` varchar(1024),
		`backed_at` TIMESTAMP,
		`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`update_at` timestamp 
	)
	;

	CREATE TABLE `notice_find` (
	    `id` INTEGER PRIMARY KEY NOT NULL,
		`uuid` varchar(255) not null,
		`name` varchar(255) not null,
		`desp` varchar(1024) not null,
		`location_sheng` varchar(1024) not null,
		`location_shi` varchar(1024) not null,
		`location_xian` varchar(1024) not null,
		`location_note` varchar(1024) not null,
		`person` varchar(1024) not null,
		`email` varchar(1024),
        --succ
        --cancel
        --timeout
		`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
	)
	;

	CREATE TABLE `notice_lost` (
	    `id` INTEGER PRIMARY KEY NOT NULL,
		`uuid` varchar(255) not null,
		`name` varchar(255) not null,
		`desp` varchar(1024) not null,
		`location_sheng` varchar(1024) not null,
		`location_shi` varchar(1024) not null,
		`location_xian` varchar(1024) not null,
		`location_note` varchar(1024) not null,
		`person` varchar(1024) not null,
		`email` varchar(1024),
        --succ
        --cancel
        --timeout
		`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
	)
	;

	CREATE TABLE `notice_thanks` (
	    `id` INTEGER PRIMARY KEY NOT NULL,
		`name` varchar(255) not null,
		`created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
	)
	;


COMMIT;
