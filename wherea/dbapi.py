#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""用来和数据库进行交互"""

import config
import copy
import datetime
import hashlib
import time
import uuid
import web
import sys

from config import db

reload(sys)
sys.setdefaultencoding('utf-8')

def notice_find_create(_uuid, name, desp, location_sheng, location_shi,
        location_xian, location_note, person, email):
    db.insert('notice_find', uuid = _uuid, name = name, desp = desp, location_sheng = location_sheng,
            location_shi = location_shi, location_xian = location_xian,
            location_note = location_note, person = person, email = email)

def notice_find_delete(id):
    db.delete('notice_find', where='id=$id', vars=locals())

def notice_find_update(id, name, desp, location_sheng, location_shi,
        location_xian, location_note, person, email):
    db.update('notice_find', where='id=$id',
            name = name,
            desp = desp,
            location_sheng = location_sheng,
            location_shi = location_shi,
            location_xian = location_xian,
            location_note = location_note,
            person = person,
            email = email,
            vars=locals())

def notice_find_get(id):
    return db.select('notice_find', where='id=$id', vars=locals())

def notice_find_get_offset(offset, limit):
    t = db.select('notice_find', where='id>=0', order="id desc", limit=limit, offset=offset)
    return t

def notice_find_get_all():
    t = db.select('notice_find', where='id>=0')
    return t

#todo for mutil names
def notice_find_get_filter(location_sheng, location_shi, location_xian, location_note, name, created_begin, created_end):
    t = db.select('notice_find', where='location_sheng="{}" and location_shi="{}" and location_xian="{}" and location_note="{}" and name like "%{}%" and created_at>="{}" and created_at<="{}"'.format(location_sheng, location_shi, location_xian, location_note, name, created_begin, created_end))
    return t

def notice_find_total():
    #todo
    i = 0
    for j in notice_find_get_all():
        i += 1
    return i

#def notice_update(id, title, content):
    #db.update('notice_find', where='id=$id', title=title, content=content, vars=locals())
#------------
def notice_lost_create(_uuid, name, desp, location_sheng, location_shi,
        location_xian, location_note, person, email):
    db.insert('notice_lost', uuid = _uuid, name = name, desp = desp, location_sheng = location_sheng,
            location_shi = location_shi, location_xian = location_xian,
            location_note = location_note, person = person, email = email)

def notice_lost_delete(id):
    db.delete('notice_lost', where='id=$id', vars=locals())

def notice_lost_update(id, name, desp, location_sheng, location_shi,
        location_xian, location_note, person, email):
    db.update('notice_lost', where='id=$id',
            name = name,
            desp = desp,
            location_sheng = location_sheng,
            location_shi = location_shi,
            location_xian = location_xian,
            location_note = location_note,
            person = person,
            email = email,
            vars=locals())

def notice_lost_get(id):
    return db.select('notice_lost', where='id=$id', vars=locals())

def notice_lost_get_offset(offset, limit):
    t = db.select('notice_lost', where='id>=0', order="id desc", limit=limit, offset=offset)
    return t

def notice_lost_get_all():
    t = db.select('notice_lost', where='id>=0')
    return t

def notice_lost_total():
    #todo
    i = 0
    for j in notice_lost_get_all():
        i += 1
    return i
#------------
def notice_create(title, content):
    db.insert('notice', title=title, content=content)

def notice_update(id, title, content):
    db.update('notice', where='id=$id', title=title, content=content, vars=locals())

def notice_back(id, back_str):
    db.update('notice', where='id=$id', is_back=True, back_str=back_str, backed_at=datetime.datetime.now(), vars=locals())

def notice_delete(id):
    db.delete('notice_find', where='id=$id', vars=locals())

def notice_get(id):
    return db.select('notice_find', where='id=$id', vars=locals())

def notice_get_offset(offset, limit):
    t = db.select('notice_find', where='id>=0', order="id desc", limit=limit, offset=offset)
    return t

def notice_get_all():
    t = db.select('notice_find', where='id>=0')
    return t

def notice_total():
    #todo
    i = 0
    for j in notice_get_all():
        i += 1
    return i

if __name__ == '__main__':
    location_sheng = u'北京'
    location_shi = u'北京'
    location_xian = u'海淀区'
    location_note = u'上地10街'
    name = u'物品'

    created_begin = "2014-07-01 01:01:16"
    created_end = "2014-07-01 01:01:18"

    created_begin = "2014-07-10 00:00:00"
    created_end = "2014-07-11 22:22:22"

    t = db.select('notice_find')
    for i in t:
        print i.name, i.location_sheng, i.created_at

    #t = db.select('notice_find', where='location_sheng="{}" and name like "%{}%" and created_at>="{}" and created_at<="{}"'.format(location_sheng, name, created_begin, created_end))

    t = notice_find_get_filter(location_sheng, location_shi, location_xian, location_note, name, created_begin, created_end)

    count = 0
    for i in t:
        print i.location_sheng, i.name, i.created_at, count
        count = count + 1

    print dir(t), t.c, t.i, t.list
    print 'x' * 10
    for i in t:
        print i.location_sheng, i.name, i.created_at
