#!/usr/bin/env python
#coding:utf-8

import json
import web
import socket
import os

path = os.path.dirname(os.path.realpath(__file__))
db_file = os.path.join(path, 'wherea.db')
sql_file = os.path.join(path, 'wherea.sql')
db = web.database(dbn='sqlite', db=db_file)
