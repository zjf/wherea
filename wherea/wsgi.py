#!/usr/bin/env python
#-*- coding:utf-8 -*-

import datetime
import json
import sys
import web
import os
import time
import shelve
import random
from subprocess import Popen,PIPE
from web.contrib.template import render_jinja
from web import form
reload(sys)
sys.setdefaultencoding('utf-8')

from wherea import config
from wherea import dbapi

web.config.debug = True
#web.config.session_parameters['timeout'] = 10 #24 * 60 * 60=86400, # 24 hours   in seconds


urls=("/hello","Hello",
    "/","Index",
    "/notice", "Notice",
    "/notice_find", "Notice_find",
    "/notice_find_grid", "Notice_find_grid",
    "/notice_find_commit", "Notice_find_commit",
    "/notice_lost", "Notice_lost",
    "/notice_lost_commit", "Notice_lost_commit",
    "/back", "Back",
    )

app=web.application(urls, globals())
webpy_render=web.template.render('templates/')
render = render_jinja('templates/', encoding='utf-8',)

_session = web.config.get('_session')
if _session is None:
    session = web.session.Session(app, 
            web.session.DiskStore('session'), initializer={'is_login':False, 'nickname':None})
    web.config._session = session
else:
    session = _session
def session_hook():
    web.ctx.session = session
app.add_processor(web.loadhook(session_hook))

class Hello:
    def GET(self):
        return 'hello!'

class Index:
    def GET(self):
        return render.index()

class Notice_find:
    def GET(self):
        x = web.input()
        rows = int(x.get('rows', 7))
        total = dbapi.notice_find_total()
        page_total = total / rows
        if not total % rows == 0:
            page_total += 1

        page = int(x.get('page', 1))
        if page < 1:
            page = 1
        if page > page_total:
            page = page_total

        offset = rows*(page-1)
        limit = rows
        notices = dbapi.notice_find_get_offset(offset, limit)
        d = datetime.datetime.now()
        days = []
        for i in range(30):
            d_txt = d.isoformat(" ").split(' ')[0]
            days.append(d_txt)
            d = d - datetime.timedelta(days =1)

        #print days
        return render.notice_find(notices=notices, total=page_total, 
                page=page, records=total, days=days)

    def POST(self):
        x = web.input()
        print x

        name = x.get('name', '')
        desp = x.get('desp', '')
        location_sheng = x.get('location_sheng', '')
        location_shi = x.get('location_shi', '')
        location_xian = x.get('location_xian', '')
        location_note = x.get('location_note', '')
        person = x.get('person', '')
        email = x.get('email', '')
        _uuid = str(uuid.uuid4())

        print location_sheng, location_shi, location_xian, location_note

        if not all([name, desp, location_sheng, location_shi, location_xian, person, email]):
            raise web.badrequest()

        dbapi.notice_find_create(_uuid, name, desp, location_sheng, location_shi, location_xian, location_note, person, email)
        return render.notice_create_ok()

class Notice_find_grid:
    def GET(self):
        x = web.input()
        print x

        location_sheng = x.get('location_sheng')
        location_shi = x.get('location_shi')
        location_xian = x.get('location_xian')
        location_note = x.get('location_note')
        name = x.get('name')
        created_begin = x.get('created_begin')
        created_end = x.get('created_end')
        rows = int(x.get('rows', 7))

        #location_sheng = u'北京'
        #location_shi = u'北京'
        #location_xian = u'海淀区'
        #location_note = u'上地10街'
        #name = u'物品'
        #created_begin = "2014-07-03 01:01:16"
        #created_end = "2014-07-03 01:01:18"

        if (location_sheng is None):
            t = dbapi.notice_find_get_all()
        else:
            created_begin = "%s 00:00:00" % (created_begin)
            created_end = "%s 23:59:59" % (created_end)
            t = dbapi.notice_find_get_filter(location_sheng, location_shi, location_xian, location_note, name, created_begin, created_end)

        notices = []
        for i in t:
            notices.append(i)

        total = t.c
        if total == 0:
            return render.notice_find_grid_notfound()

        page_total = total / rows
        if not total % rows == 0:
            page_total += 1

        page = int(x.get('page', 1))
        if page < 1:
            page = 1
        if page > page_total:
            page = page_total

        offset = rows * (page-1)
        limit = rows

        notices = notices[offset:offset+limit]
        print notices, offset, limit, page, rows, total
        return render.notice_find_grid(notices=notices, total=page_total, 
                page=page, records=total)

class Notice_find_commit:
    def GET(self):
        return render.notice_find_commit()

class Notice_lost_commit:
    def GET(self):
        return render.notice_lost_commit()

class Notice_lost:
    def GET(self):
        x = web.input()
        rows = int(x.get('rows', 7))
        total = dbapi.notice_lost_total()
        page_total = total / rows
        if not total % rows == 0:
            page_total += 1

        page = int(x.get('page', 1))
        if page < 1:
            page = 1
        if page > page_total:
            page = page_total

        offset = rows*(page-1)
        limit = rows
        notices = dbapi.notice_lost_get_offset(offset, limit)
        return render.notice_lost_grid(notices=notices, total=page_total, 
                page=page, records=total)

    def POST(self):
        x = web.input()
        print x

        name = x.get('name', '')
        desp = x.get('desp', '')
        location_sheng = x.get('location_sheng', '')
        location_shi = x.get('location_shi', '')
        location_xian = x.get('location_xian', '')
        location_note = x.get('location_note', '')
        person = x.get('person', '')
        email = x.get('email', '')
        _uuid = str(uuid.uuid4())

        print location_sheng, location_shi, location_xian, location_note

        if not all([name, desp, location_sheng, location_shi, location_xian, person, email]):
            raise web.badrequest()

        dbapi.notice_lost_create(_uuid, name, desp, location_sheng, location_shi, location_xian, location_note, person, email)
        return render.notice_create_ok()

class Notice:
    def GET(self):
        x = web.input()
        rows = int(x.get('rows', 7))
        total = dbapi.notice_total()
        page_total = total / rows
        if not total % rows == 0:
            page_total += 1

        page = int(x.get('page', 1))
        if page < 1:
            page = 1
        if page > page_total:
            page = page_total

        offset = rows*(page-1)
        limit = rows
        notices = dbapi.notice_get_offset(offset, limit)
        return render.notice_grid(notices=notices, total=page_total, 
                page=page, records=total)

    def POST(self):
        x = web.input()
        title = x.get('title', '')
        content = x.get('content', '')
        contact = x.get('contact', '')
        content +=  (' ' + contact)
        if not all([title, content]):
            raise web.badrequest()
        dbapi.notice_create(title, content)
        return render.notice_create_ok()

class Back:
    def GET(self):
        x = web.input()
        notice_id = x.get('notice_id')
        notice = dbapi.notice_get(notice_id) 
        if notice is None:
            raise web.badrequest()
        return render.back(notice_id=notice_id)

    def POST(self):
        x = web.input()
        notice_id = x.get('notice_id')
        back_str = x.get('back_str')
        dbapi.notice_back(notice_id, back_str)
        return 'ok'

if __name__ == "__main__":
    #web.config.debug = False
    app.run()
else:
    web.config.debug = False
    application = app.wsgifunc()
