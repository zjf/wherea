$('document').ready(function(){
 


/**
 * 获取鼠标在页面上的位置
 * @param ev		触发的事件
 * @return			x:鼠标在页面上的横向位置, y:鼠标在页面上的纵向位置
 */
function getMousePoint(ev) {
	// 定义鼠标在视窗中的位置
	var point = {
		x:0,
		y:0
	};
 
	// 如果浏览器支持 pageYOffset, 通过 pageXOffset 和 pageYOffset 获取页面和视窗之间的距离
	if(typeof window.pageYOffset != 'undefined') {
		point.x = window.pageXOffset;
		point.y = window.pageYOffset;
	}
	// 如果浏览器支持 compatMode, 并且指定了 DOCTYPE, 通过 documentElement 获取滚动距离作为页面和视窗间的距离
	// IE 中, 当页面指定 DOCTYPE, compatMode 的值是 CSS1Compat, 否则 compatMode 的值是 BackCompat
	else if(typeof document.compatMode != 'undefined' && document.compatMode != 'BackCompat') {
		point.x = document.documentElement.scrollLeft;
		point.y = document.documentElement.scrollTop;
	}
	// 如果浏览器支持 document.body, 可以通过 document.body 来获取滚动高度
	else if(typeof document.body != 'undefined') {
		point.x = document.body.scrollLeft;
		point.y = document.body.scrollTop;
	}
 
	// 加上鼠标在视窗中的位置
	point.x += ev.clientX;
	point.y += ev.clientY;
 
	// 返回鼠标在视窗中的位置
	return point;
}

/**
     * 添加事件
     * @param node 监听对象
     * @param type 监听类型
     * @param listener 触发事件
     * @return 事件是否添加成功
     */
function addEvent(node, type, listener) {
     if(node.addEventListener) {
     node.addEventListener(type, listener, false);
     return true;
     } else if(node.attachEvent) {
     node['e' + type + listener] = listener;
     node[type + listener] = function() {
     node['e' + type + listener](window.event);
     };
     node.attachEvent('on' + type, node[type + listener]);
     return true;
     }
     return false;
} 

function add_marker(map, point)
{
	 var marker = new BMap.Marker(point);        // 创建标注  
		map.addOverlay(marker);  
		$("#lng").attr("value", point.lng);
		$("#lat").attr("value", point.lat);
		//alert(point.lng);
		marker.addEventListener("click", function(e){  
			alert('当前位置:'+e.point.lng+','+e.point.lat);
			add_opts(map, point);
			if (confirm('确定删除所选内容?')){
				alert('左边展开了该组信息');
			}
			else{
			}
			return false;
		});
}

//创建marker, 使用自定义的数据创建标示
window.addMarker = function (data){
    //map.clearOverlays();
    for(var i=0;i<data.length;i++){
        var json = data[i];
        var lng = json.lng;
        var lat = json.lat;
        var point = new BMap.Point(lng, lat);
				var myIcon = new BMap.Icon( 
						"http://openapi.baidu.com/map/images/custom_a_j.png",new BMap.Size(28, 37),
						{
							     offset: new BMap.Size(10, 25),
									 imageOffset: new BMap.Size()
						});
				var marker = new BMap.Marker(point, {icon: myIcon});
        var label = new BMap.Label(json.name,{"offset":new BMap.Size(5-6+10, -20)});
        marker.setLabel(label);
        map.addOverlay(marker);
				label.setStyle({
            borderColor:"#808080",
            color:"#333",
            cursor:"pointer"
        });
        (function(){
            var _json = json;
            var _iw = createInfoWindow(_json);
            var _marker = marker;
            _marker.addEventListener("click",function(){
                this.openInfoWindow(_iw);
               });
               _iw.addEventListener("open",function(){
                _marker.getLabel().hide();
               })
               _iw.addEventListener("close",function(){
                _marker.getLabel().show();
               })
            label.addEventListener("click",function(){
                _marker.openInfoWindow(_iw);
               })
            if(!!0){
             label.hide();
             _marker.openInfoWindow(_iw);
            }
				})
				()
		}
}

//创建InfoWindow
function createInfoWindow(json){
    var iw = new BMap.InfoWindow("<div class='iw_poi_title' title='" + json.name + "'>" + json.name+"<br/>"+json.note+"<input id='"+json.id+"' value='加入' class='join_channel' type='submit'/></div>");
    return iw;
}

function add_opts(map, point)
{
		var opts = {  
			width : 250,     // 信息窗口宽度  
			height: 100,     // 信息窗口高度  
			title : "Hello"  // 信息窗口标题  
		}  
		var infoWindow = new BMap.InfoWindow("World _zz2", opts);  // 创建信息窗口对象  
		map.openInfoWindow(infoWindow, point);
}
	
function add_click(map)
{
	map.addEventListener("click", function(e){
		 //alert("您标注的位置：" +  e.point.lng + ", " + e.point.lat); 
		 var point = new BMap.Point(e.point.lng, e.point.lat);  
		 var statu = $("#marker_statu").attr("statu");
		 if (statu == "open"){
		 		 add_marker(map, point);
		 }
	});
}


function add_menu(map)
{	
		var menu = new BMap.ContextMenu();
		var txtMenuItem = [
				{
						 text:'放大',
											 callback:function(){map.zoomIn()}
										},
				{
						 text:'缩小',
										 callback:function(){map.zoomOut()}
										},
				{
						 text:'创建',
										 callback:function(){
												$("#lng").attr("value", $("#label_point").attr("lng"));
												$("#lat").attr("value", $("#label_point").attr("lat"));
												var channel_create_div = document.getElementById('channel_create_div');
												var top = $("#label_topleft").attr("top");
												var left = $("#label_topleft").attr("left");
												channel_create_div.style.left = left + 'px';
												channel_create_div.style.top = top + 'px';
												$("#channel_create_div").show();
										 }
										}
		 ];

		for(var i=0; i < txtMenuItem.length; i++){
				menu.addItem(new BMap.MenuItem(txtMenuItem[i].text,txtMenuItem[i].callback,100));
		}
		menu.addEventListener("open", function(e){
				$("#label_point").attr("lng", e.point.lng);
				$("#label_point").attr("lat", e.point.lat);
				$("#label_point").attr("who", "open");
				//alert(e.point.lng+e.point.lat);
		});
		map.addContextMenu(menu);
}

function set_label_centerpoint(point)
{
		var lng = point.lng;
		var lat = point.lat;
		//alert("in set");
		//alert(lng+lat);
		$("#label_centerpoint").attr("lng", lng);
		$("#label_centerpoint").attr("lat", lat);
}

function get_label_centerpoint()
{
		lng = $("#label_centerpoint").attr("lng");
		lat = $("#label_centerpoint").attr("lat");
		//alert("in get");
		//alert(lng+lat);
		var point = new BMap.Point(lng, lat);  
		return point;
}

//根据points，服务器端返回这些points周围的点
function get_data_byPoints(points)
{
	var lng_lat = [];
  for(var i=0;i<points.length;i++){
		var lng = points[i].lng;
		var lat = points[i].lat;
		lng_lat.push(lng+"|"+lat);
	}
	var str = lng_lat.join(";");
	var result = "";
	$.get("/getData", {lng_lat:str}, function(data){
			result = data.rows;
	});
	alert("这个alert不得不加上，不加上的return 语句会在$get执行完成之前就return result。的不到想要的值");
	return result;
}


// 添加标注, 使用百度地图的数据创建标示
function _addMarker(point, index){
  var myIcon = new BMap.Icon("http://api.map.baidu.com/img/markers.png", new BMap.Size(23, 25), {
    offset: new BMap.Size(10, 25),
    imageOffset: new BMap.Size(0, 0 - index * 25)
  });
  var marker = new BMap.Marker(point, {icon: myIcon});
  map.addOverlay(marker);
  return marker;
}
   

 // 添加信息窗口
function addInfoWindow(marker,poi,index){
    var maxLen = 10;
    var name = null;
    if(poi.type == BMAP_POI_TYPE_NORMAL){
        name = "地址：  "
    }else if(poi.type == BMAP_POI_TYPE_BUSSTOP){
        name = "公交：  "
    }else if(poi.type == BMAP_POI_TYPE_SUBSTOP){
        name = "地铁：  "
    }
    // infowindow的标题
    var infoWindowTitle = '<div style="font-weight:bold;color:#CE5521;font-size:14px">'+poi.title+'</div>';
    // infowindow的显示信息
    var infoWindowHtml = [];
    infoWindowHtml.push('<table cellspacing="0" style="table-layout:fixed;width:100%;font:12px arial,simsun,sans-serif"><tbody>');
    infoWindowHtml.push('<tr>');
    if(name != null){
        infoWindowHtml.push('<td style="vertical-align:top;line-height:16px;width:38px;white-space:nowrap;word-break:keep-all">' + name + '</td>');
    }
    infoWindowHtml.push('<td style="vertical-align:top;line-height:16px">' + poi.address + ' </td>');
    infoWindowHtml.push('</tr>');
    infoWindowHtml.push('</tbody></table>');
    var infoWindow = new BMap.InfoWindow(infoWindowHtml.join(""),{title:infoWindowTitle,width:200}); 
    var openInfoWinFun = function(){
        marker.openInfoWindow(infoWindow);
        for(var cnt = 0; cnt < maxLen; cnt++){
            if(!document.getElementById("list" + cnt)){continue;}
            if(cnt == index){
                document.getElementById("list" + cnt).style.backgroundColor = "#f0f0f0";
            }else{
                document.getElementById("list" + cnt).style.backgroundColor = "#fff";
            }
        }
    }
    marker.addEventListener("click", openInfoWinFun);
    return openInfoWinFun;
}


	$("#search_submit").livequery("click", function(){
		var options = {
				onSearchComplete: function(results){
						map.clearOverlays();
            // 判断状态是否正确
            if (local.getStatus() == BMAP_STATUS_SUCCESS){
                var s = [];
                s.push('<div style="font-family: arial,sans-serif; border: 1px solid rgb(153, 153, 153); font-size: 12px;">');
                s.push('<div style="background: none repeat scroll 0% 0% rgb(255, 255, 255);">');
                s.push('<ol style="list-style: none outside none; padding: 0pt; margin: 0pt;">');
                openInfoWinFuns = [];
								points = [];
                for (var i = 0; i < results.getCurrentNumPois(); i ++){
                    var marker = _addMarker(results.getPoi(i).point,i);
                    var openInfoWinFun = addInfoWindow(marker,results.getPoi(i),i);
                    openInfoWinFuns.push(openInfoWinFun);
										points.push(results.getPoi(i).point);
                    // 默认打开第一标注的信息窗口
                    var selected = "";
                    if(i == 0){
                        selected = "background-color:#f0f0f0;";
                        openInfoWinFun();
                    }
                    s.push('<li id="list' + i + '" style="margin: 2px 0pt; padding: 0pt 5px 0pt 3px; cursor: pointer; overflow: hidden; line-height: 17px;' + selected + '" onclick="openInfoWinFuns[' + i + ']()">');
                    s.push('<span style="width:1px;background:url(http://api.map.baidu.com/bmap/red_labels.gif) 0 ' + ( 2 - i*20 ) + 'px no-repeat;padding-left:10px;margin-right:3px"> </span>');
                    s.push('<span style="color:#00c;text-decoration:underline">' + results.getPoi(i).title.replace(new RegExp(results.keyword,"g"),'<b>' + results.keyword + '</b>') + '</span>');
                    s.push('<span style="color:#666;"> - ' + results.getPoi(i).address + '</span>');
                    s.push('</li>');
                    s.push('');
                }
                s.push('</ol></div></div>');
                document.getElementById("results").innerHTML = s.join("");
								var data = get_data_byPoints(points);
								//alert(data);
								addMarker(data);
								//alert('here wait');
            }
          }
        };
        
			var search_name = $("#search_name").val();		 
			var local = new BMap.LocalSearch(map, options);
			//var local = new BMap.LocalSearch(map, {renderOptions:{map: map}});
			local.search(search_name);
		});

	$(".marker_switch").livequery("click", function(){
			var statu = $(this).attr("statu");
			alert(statu);
			$("#marker_statu").attr("statu", statu);
		});

	$(".join_channel").livequery("click", function(){
			var id = $(this).attr("id");
			$("#chat").load("/index2/"+id);
		});

	$("#show_channel").livequery("click", function(){
			alert('显示当前页面上可用到的讨论组');
			var points = []
			var point = map.getCenter();
			points.push(point);
			var data = get_data_byPoints(points);
			//alert(data);
			addMarker(data);
		});


	$("#channel_create_submit").livequery("click", function(){
			var lng = $("#lng").val();
			var lat = $("#lat").val();
			var note = $("#note").val();
			var channel = $("#channel").val();
			//alert("click submit");
			//alert(lng+lat+channel+note);
			$.post("/createChannel", 
				{"lng":lng, "lat":lat, "channel":channel, "note":note}, 
				function(data){
					if (data.status == "ok")
					{
						t = [{"name": channel, "founder": data.founder, "note": note, "lat": lat, "lng":lng , "id": data.id}];
						addMarker(t);
					}
			});
			$("#channel_create_div").hide();
		});

	var container = document.getElementById('container');
	var label_topleft = $("#label_topleft");
  addEvent(container, 'mousemove', function(ev){
			var cursorPos = getMousePoint(ev);
			label_topleft.attr("top", cursorPos.y);
			label_topleft.attr("left", cursorPos.x);
	});


	window.map = new BMap.Map("container");  
	var point = new BMap.Point(116.404, 39.915);  
	set_label_centerpoint(point);
	map.centerAndZoom(point, 15); 
	add_menu(map);

});
