$('document').ready(function(){
		$("#submitmsg").livequery("click", function(){
				var channel_id = $("#channel_label").attr("channel_id");
				var user = $("#channel_label").attr("user");
				var clientmsg = $("#usermsg").val();
				//alert(channel_id);
				//alert(user);
				//alert(clientmsg);
				$.post("/post?channel_id="+channel_id+"&user="+user, {text: clientmsg});
				$("#usermsg").attr("value", "");
				return false;
		});

		function loadLog(){		
				var channel_id = $("#channel_label").attr("channel_id");
    		$.ajax({
    				url: "/post?channel_id="+channel_id,
    				cache: false,
    				success: function(html){
								var old_html = $("#chatbox").html();
								//alert(old_html);
								var html = old_html+html;
    						$("#chatbox").html(html); //Insert chat log into the #chatbox div
    				},
    		});
		}

		function loadMem(){		
				var channel_id = $("#channel_label").attr("channel_id");
    		$.ajax({
    				url: "/getMem?channel_id="+channel_id,
    				cache: false,
    				success: function(html){
    						$("#mem").html(html); //Insert成员列表 into the #chatbox div
    				},
    		});
		}
		setInterval(loadLog, 2500);
		setInterval(loadMem, 2500);

		$("#exit").livequery("click", function(){
				var channel_id = $("#channel_label").attr("channel_id");
    		$.ajax({
    				url: "/leaveChannel?channel_id="+channel_id,
    				cache: false,
    				success: function(data){
							alert(data);
    				},
    		});
				$("chat").html()='';
		});

});
