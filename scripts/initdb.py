#!/usr/bin/python
#-*- coding: utf-8 -*-

import os
import sqlite3
from wherea.config import db_file, sql_file

if not os.path.exists(db_file):
    conn = sqlite3.connect(db_file)
    cursor = conn.cursor()
    cursor.executescript(open(sql_file).read())
    conn.commit()
    cursor.close()
    conn.close()

if __name__ == '__main__':
    pass


