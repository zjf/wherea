#!/usr/bin/python
#-*- coding: utf-8 -*-

import os
import uuid
import sqlite3

from wherea.config import db_file, sql_file
from wherea import dbapi

#create some notice_find
notice_find_count = 100
for i in range(notice_find_count):
    _uuid = str(uuid.uuid4())
    name = u'物品_%d' % (i)
    desp = u'黑色'
    location_sheng = u'北京'
    location_shi = u'北京'
    location_xian = u'海淀区'
    location_note = u'上地10街'
    person = 'zz%d' % (i)
    email = 'zz%d@135%d.com' % (i, i)
    dbapi.notice_find_create(_uuid, name, desp, location_sheng, location_shi,
            location_xian, location_note, person, email)

notice_lost_count = 100
for i in range(notice_lost_count):
    _uuid = str(uuid.uuid4())
    name = u'物品_%d' % (i)
    desp = u'黑色'
    location_sheng = u'北京'
    location_shi = u'北京'
    location_xian = u'海淀区'
    location_note = u'上地10街'
    person = 'zz%d' % (i)
    email = 'zz%d@135%d.com' % (i, i)
    dbapi.notice_lost_create(_uuid, name, desp, location_sheng, location_shi,
            location_xian, location_note, person, email)


if __name__ == '__main__':
    pass
